# due-date-calculator

## Getting started

```
git clone https://gitlab.com/martonszel/due-date-calculator.git
cd due-date-calculator
npm i
to start the application : node CalculateDueDate.js
to start the test : npm run test
```

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/martonszel/due-date-calculator.git
git branch -M main
git push -uf origin main
```

---
