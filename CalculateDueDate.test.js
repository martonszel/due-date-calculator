
const CalculateDueDate = require('./CalculateDueDate')
const isWeekend = require('./CalculateDueDate')
const isWorkingHour = require('./CalculateDueDate')

describe('CalculateDueDate TDD', () => {

    describe('CalculateDueDate function exists', () => {
        test('should pass', () => {
            expect(CalculateDueDate.CalculateDueDate).toBeDefined()
        });
    })

    describe('check parameters', () => {
        test('should pass ', () => {
            const date = new Date(Date.UTC(2022, 2, 24, 10, 59));
            const endDate = new Date(Date.UTC(2022, 2, 28, 10, 59));
            expect(CalculateDueDate.CalculateDueDate(date, 16)).toStrictEqual(endDate);
        });
    })

    describe('Invalid submitDate', () => {
        test('should pass ', () => {
            expect(() => CalculateDueDate.CalculateDueDate('2015-02-16', 16)).toThrow("Invalid Submit Date");
        });
    })

    describe('Invalid submitDate', () => {
        test('should pass ', () => {
            expect(() => CalculateDueDate.CalculateDueDate(45689, 16)).toThrow("Invalid Submit Date");
        });
    })

    describe('Invalid submitDate', () => {
        test('should pass ', () => {
            expect(() => CalculateDueDate.CalculateDueDate('45689', 16)).toThrow("Invalid Submit Date");
        });
    })

    describe('Invalid turnaround time', () => {
        test('should pass ', () => {
            const date = new Date(Date.UTC(2022, 2, 24, 10, 59));
            expect(() => CalculateDueDate.CalculateDueDate(date, 'string')).toThrow("Invalid Turnaround Time");
        });
    })

    describe('Invalid turnaround time', () => {
        test('should pass ', () => {
            const date = new Date(Date.UTC(2022, 2, 24, 10, 59));
            expect(() => CalculateDueDate.CalculateDueDate(date, -25)).toThrow("Invalid Turnaround Time");
        });
    })

    describe('Invalid turnaround time', () => {
        test('should pass ', () => {
            const date = new Date(Date.UTC(2022, 2, 24, 10, 59));
            expect(() => CalculateDueDate.CalculateDueDate(date, ['string', 'number'])).toThrow("Invalid Turnaround Time");
        });
    })

    describe('Invalid turnaround time', () => {
        test('should pass ', () => {
            const date = new Date(Date.UTC(2022, 2, 24, 10, 59));
            expect(() => CalculateDueDate.CalculateDueDate(date, true || false)).toThrow("Invalid Turnaround Time");
        });
    })

    describe('isWeekend function exists', () => {
        test('should pass', () => {
            expect(CalculateDueDate.isWeekend).toBeDefined()
        });
    })

    describe('isWorkingHour function exists', () => {
        test('should pass', () => {
            expect(CalculateDueDate.isWorkingHour).toBeDefined()
        });
    })

    describe('isWorkingHour function exists', () => {
        test('should pass', () => {
            const date = new Date(Date.UTC(2022, 2, 24, 10, 59));
            expect(CalculateDueDate.isWorkingHour(date)).toBe(true)
        });
    })


    describe('Friday 20 hours', function () {
        test('should be equal ', function () {
            const taskDate = new Date(Date.UTC(2022, 2, 25, 9, 0));
            const date = CalculateDueDate.CalculateDueDate(taskDate, 20);
            const dueDate = new Date(Date.UTC(2022, 2, 29, 13, 0));
            expect(dueDate.getTime()).toEqual(date.getTime());
        });
    });


    describe('Weekday 17pm 8hours ', function () {
        test('should be equal ', function () {
            const taskDate = new Date(Date.UTC(2022, 2, 23, 16, 59));
            const date = CalculateDueDate.CalculateDueDate(taskDate, 8);
            const dueDate = new Date(Date.UTC(2022, 2, 24, 16, 59));
            expect(dueDate.getTime()).toEqual(date.getTime());
        });
    });

    describe('Zero hours', function () {
        it('should be equal ', function () {
            const taskDate = new Date(Date.UTC(2022, 2, 23, 16, 30));
            const date = CalculateDueDate.CalculateDueDate(taskDate, 0);
            const dueDate = new Date(Date.UTC(2022, 2, 23, 16, 30));
            expect(dueDate.getTime()).toEqual(date.getTime());
        });
    });

    describe('0.5 hours', function () {
        it('should be equal ', function () {
            const taskDate = new Date(Date.UTC(2022, 2, 23, 16, 0));
            const date = CalculateDueDate.CalculateDueDate(taskDate, 0.5);
            const dueDate = new Date(Date.UTC(2022, 2, 23, 16, 30));
            expect(dueDate.getTime()).toEqual(date.getTime());
        });
    });



});