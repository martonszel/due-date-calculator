'use strict'

const startWorkingHours = 9;
const endWorkingHours = 17;

const validSubmitDate = (submitDate) => submitDate instanceof Date

const validTurnaroundTime = (turnaroundTime) => !isNaN(turnaroundTime) && typeof turnaroundTime != 'boolean' && turnaroundTime != null && turnaroundTime >= 0 && typeof turnaroundTime != 'string'

const isWeekend = (submitDate) => submitDate.getDay() > 0 && submitDate.getDay() < 6

const isWorkingHour = (submitDate) => {
    return submitDate.getHours() >= startWorkingHours && submitDate.getHours() <= endWorkingHours
}

const addDay = (date) => {
    date.setDate(date.getDate() + 1);
}

const addWorkingDays = (date, days) => {
    while (days) {
        addDay(date);
        if (isWeekend(date)) {
            --days;
        }
    }
}

const setNextWorkDay = (date) => {
    addWorkingDays(date, 1);
    date.setUTCHours(startWorkingHours);
    date.setUTCMinutes(0);
}

const getRemaingWorkTime = (date) => {
    const day = new Date(date);
    day.setUTCHours(endWorkingHours);
    day.setUTCMinutes(0);
    return day.getTime() - date.getTime();
}

const CalculateDueDate = (submitDate, turnaroundTime) => {

    console.log(submitDate);

    if (!validSubmitDate(submitDate)) {
        throw new Error("Invalid Submit Date");
    }

    if (!isWeekend(submitDate)) {
        throw new Error("SubmitDate cant be weekend");
    }

    if (!isWorkingHour(submitDate)) {
        throw new Error("SubmitDate Hours only between 9-17");
    }

    if (!validTurnaroundTime(turnaroundTime)) {
        throw new Error("Invalid Turnaround Time");
    }

    // convert hours to milliseconds
    let milliseconds = turnaroundTime * 60 * 60 * 1000;

    while (milliseconds > 0) {
        let remainingWorkTime = getRemaingWorkTime(submitDate);
        let workTime = Math.min(remainingWorkTime, milliseconds);

        submitDate.setTime(submitDate.getTime() + workTime);

        milliseconds -= workTime;
        if (milliseconds > 0) {
            setNextWorkDay(submitDate);
        }
    }
    console.log(submitDate);
    return submitDate;
};

const startDate = new Date(Date.UTC(2022, 2, 24, 10));
const endDate = 16

CalculateDueDate(startDate, endDate)

module.exports = { CalculateDueDate, isWeekend, getRemaingWorkTime, isWorkingHour }
